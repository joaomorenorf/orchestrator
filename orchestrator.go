package orchestrator

import (
	"fmt"
	"runtime"
	"time"
)

type Orchestrator struct {
	cores int
}

func NewOrchestrator() (o *Orchestrator) {
	o = new(Orchestrator)
	o.cores = runtime.NumCPU()
	return o
}

// Process tasks untill channel is empty and closed
func processor(id string, chi chan string, cho chan string) {
	for {
		value, wait := <-chi
		if !wait {
			break
		}
		// The task is just wait
		time.Sleep(1 * time.Second)
		// Send a message after a task is done
		cho <- "task: " + value + " done by process: " + id
	}
	// Send a message when finishing
	cho <- "closing process " + id
}

// Create n tasks
func taskCreator(n int, chi chan string, cho chan string) {
	cho <- "Number of tasks: " + fmt.Sprint(n)
	for path := 0; path < n; path++ {
		chi <- fmt.Sprint(path)
	}
	close(chi)
	cho <- "closing task creator"
}

func (threads *Orchestrator) Orchestrate(numTasks int, iChanSize int, oChanSize int) {
	// Create two channels to send and receive data from processes
	chi := make(chan string, iChanSize) // processor input channel
	cho := make(chan string, oChanSize) // processor output channel

	// Launches one thread to create the tasks and line them in the input channel
	go taskCreator(numTasks, chi, cho)

	// Launch one processor for each CPU and gives them a unique id
	for id := 0; id < threads.cores; id++ {
		go processor(fmt.Sprint(id), chi, cho)
	}

	// Receive outputs and print them in real time
	for ii := 0; ii < numTasks+threads.cores+1; ii++ {
		fmt.Println(<-cho)
	}
}
