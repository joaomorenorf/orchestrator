package orchestrator

import (
	"testing"
)

func TestOrchestrate(t *testing.T) {
	o := NewOrchestrator()
	// test with one task
	o.Orchestrate(1, 8, 8)
	// test with 8 tasks
	o.Orchestrate(8, 8, 8)
	// test with 12 tasks
	o.Orchestrate(12, 8, 8)
}
